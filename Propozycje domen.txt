Aktualne domeny: agutowski.com, gutowski.net.

Poniżej wpisujcie propozycje domen na śpiewnik.
Nie muszą być dobrej jakości, im więcej propozycji tym lepiej.
Mogłyby się kojarzyć ze śpiewaniem, gitarą, Kaczmarskim albo czymkolwiek takim.
Dostępność domen można sprawdzić tutaj: https://instantdomainsearch.com/.
Jeśli wyświetla się opcja "Buy", to znaczy, że domena jest wolna.

śpiewnik.pl (można tworzyć domeny z polskimi znakami, ale prawdę mówiąc, jeszcze takiej nie widziałem)
przyognisku.pl
nabema.pl
spiewajmy.pl (zajęte)
piewajmy.pl
piejmy.pl
jaktuniekrzyczec.pl
hycopodłogę.pl
songbookthegreat.pl
piosenkipowidełki.gov
nagłosy.pl
kapodajster.net
cosiestałoznaszątonacja.info
zmieńtonację.pl
piosenkizpierwszejreki.com
goodowski.net
hymnywieczorusobotniego.pl
doda(j)akustycznie.com
natchnionyimłody.business
podrzyjape.pl
krzycze.com krzycze.pl
piesniasli.pl (pieśnią sił)
bliskoswit.pl (blisko świt)
kaczmarskispiewnik.pl
======
mynextsong.com
picknextsong.pl 	picknextsong.com
melodieux.pl
ournextsong.pl  	ournextsong.com
ournexttune.pl  	ournexttune.com
oursongpicker.pl  oursongpicker.com
oursinging.pl 		oursinging.com
singinghour.pl
